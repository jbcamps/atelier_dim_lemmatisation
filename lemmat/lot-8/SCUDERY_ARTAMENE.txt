token	lemma	POS	morph	treated
Comme	comme	CONsub	MORPH=empty	Comme
ils	il	PROper	PERS.=3|NOMB.=s|GENRE=m|CAS=n	ils
furent	être	VERcjg	MODE=ind|TEMPS=pst|PERS.=3|NOMB.=s	furent
assez	assez	ADVgen	MORPH=empty	assez
proches	proche	ADJqua	NOMB.=s|GENRE=m	proches
,	,	PONfbl	MORPH=empty	,
pour	pour	PRE	MORPH=empty	pour
se	se	PROper	PERS.=3|NOMB.=s|CAS=r	se
servir	servir	VERinf	MORPH=empty	servir
de	de	PRE	MORPH=empty	de
leurs	leur	DETpos	PERS.=3|NOMB.=p|GENRE=m	leurs
javelots	javelot	NOMcom	NOMB.=s|GENRE=m	javelots
,	,	PONfbl	MORPH=empty	,
ils	il	PROper	PERS.=1|NOMB.=s|CAS=n	ils
les	il	PROper	PERS.=3|NOMB.=p|GENRE=m	les
lancèrent	lancer	VERcjg	MODE=ind|PERS.=3|NOMB.=s	lancèrent
avec	avec	PRE	MORPH=empty	avec
tant	tant	ADVgen	MORPH=empty	tant
de	de	PRE	MORPH=empty	de
violence	violence	NOMcom	NOMB.=s|GENRE=f	violence
,	,	PONfbl	MORPH=empty	,
que	que	CONsub	MORPH=empty	que
de	de	PRE	MORPH=empty	de
tous	tout	DETind	NOMB.=p|GENRE=m	tous
les	le	DETdef	NOMB.=p|GENRE=m	les
deux	deux	ADJcar	NOMB.=p|GENRE=f	deux
partis	parti	NOMcom	NOMB.=s|GENRE=m	partis
ces	ce	DETdem	NOMB.=p|GENRE=m	ces
armes	arme	NOMcom	NOMB.=s|GENRE=m	armes
volantes	volant	VERppa	NOMB.=p	volantes
firent	faire	VERcjg	NOMB.=s	firent
un	un	DETndf	NOMB.=s|GENRE=m	un
assez	assez	ADVgen	NOMB.=s	assez
grand	grand	ADJqua	NOMB.=s|GENRE=m	grand
effet	effet	NOMcom	NOMB.=s|GENRE=m	effet
:	:	PONfbl	MORPH=empty	:
Mais	mais	CONcoo	MORPH=empty	Mais
beaucoup	beaucoup	ADVgen	MORPH=empty	beaucoup
plus	plus	ADVgen	MORPH=empty	plus
grand	grand	ADJqua	NOMB.=s|GENRE=m	grand
sur	sur	PRE	MORPH=empty	sur
les	le	DETdef	NOMB.=p|GENRE=m	les
Capadociens	capadocien	NOMcom	NOMB.=s|GENRE=m	Capadociens
que	que	CONsub	MORPH=empty	que
sur	sur	PRE	MORPH=empty	sur
les	le	DETdef	NOMB.=p|GENRE=m	les
autres	autre	PROind	NOMB.=s|GENRE=f	autres
.	.	PONfrt	MORPH=empty	.
En	en	PRE	MORPH=empty	En
suite	suite	NOMcom	NOMB.=s|GENRE=f	suite
ayant	avoir	VERppa	MORPH=empty	ayant
mis	mettre	VERppe	NOMB.=s|GENRE=m	mis
l'	le	DETdef	NOMB.=s	l
épée	épée	NOMcom	NOMB.=s|GENRE=m	épée
à	à	PRE	MORPH=empty	à
la	le	DETdef	NOMB.=s|GENRE=f	la
main	main	NOMcom	NOMB.=s|GENRE=f	main
,	,	PONfbl	MORPH=empty	,
et	et	CONcoo	MORPH=empty	et
s'	s	DETndf	MORPH=empty	s
étant	être	VERppa	NOMB.=s|GENRE=m	étant
couverts	couvrir	VERppe	NOMB.=p|GENRE=m	couverts
de	de	PRE	MORPH=empty	de
leurs	leur	DETpos	PERS.=3|NOMB.=p|GENRE=m	leurs
boucliers	bouclier	NOMcom	NOMB.=s|GENRE=f	boucliers
;	;	PONfbl	MORPH=empty	;
ils	il	PROper	PERS.=1|NOMB.=s|GENRE=m|CAS=n	ils
commencèrent	commencer	VERcjg	MODE=ind|PERS.=3|NOMB.=s	commencèrent
de	de	PRE	MORPH=empty	de
se	se	PROper	PERS.=3|NOMB.=s|CAS=r	se
mêler	mêler	VERinf	MORPH=empty	mêler
:	:	PONfbl	MORPH=empty	:
et	et	CONcoo	MORPH=empty	et
Artamène	Artamène	NOMpro	MORPH=empty	Artamène
,	,	PONfbl	MORPH=empty	,
à	à	PRE	MORPH=empty	à
ce	ce	PROdem	NOMB.=s|GENRE=n	ce
que	que	PROrel	MORPH=empty	que
nous	nous	PROper	PERS.=2|NOMB.=p	nous
avons	avoir	VERcjg	MODE=ind|TEMPS=pst|PERS.=1|NOMB.=s	avons
su	savoir	VERppe	NOMB.=s|GENRE=m	su
,	,	PONfbl	MORPH=empty	,
immola	immoler	VERcjg	NOMB.=s|GENRE=m	immola
la	le	DETdef	NOMB.=s|GENRE=f	la
première	premier	ADJord	NOMB.=s|GENRE=f	première
victime	victime	NOMcom	NOMB.=s|GENRE=f	victime
de	de	PRE	MORPH=empty	de
ce	ce	DETdem	NOMB.=s|GENRE=m	ce
sacrifice	sacrifice	NOMcom	NOMB.=s|GENRE=m	sacrifice
sanglant	sanglant	ADJqua	NOMB.=s|GENRE=m	sanglant
.	.	PONfrt	MORPH=empty	.
Car	car	CONcoo	MORPH=empty	Car
ayant	avoir	VERppa	MORPH=empty	ayant
devancé	devancer	VERppe	NOMB.=s|GENRE=m	devancé
tous	tout	DETind	NOMB.=p|GENRE=m	tous
ses	son	DETpos	PERS.=3|NOMB.=p|GENRE=m	ses
compagnons	compagnon	NOMcom	NOMB.=p|GENRE=m	compagnons
de	de	PRE	MORPH=empty	de
quelques	quelque	DETrel	NOMB.=s|GENRE=m	quelques
pas	pas	NOMcom	GENRE=m	pas
,	,	PONfbl	MORPH=empty	,
il	il	PROper	PERS.=3|NOMB.=s|GENRE=m|CAS=n	il
tua	tuer	VERcjg	MODE=ind|PERS.=3|NOMB.=s	tua
d'	d	PONpga	MORPH=empty	d
un	un	DETndf	NOMB.=s|GENRE=m	un
grand	grand	ADJqua	NOMB.=s|GENRE=m	grand
coup	coup	NOMcom	NOMB.=s|GENRE=m	coup
d'	d	PRE.DETdef	MORPH=empty	d
épée	épée	NOMcom	NOMB.=s|GENRE=m	épée
le	le	DETdef	NOMB.=s|GENRE=m	le
premier	premier	ADJord	NOMB.=s	premier
qui	qui	PROrel	MORPH=empty	qui
lui	il	PROper	PERS.=1|NOMB.=s|CAS=i	lui
résista	résister	VERcjg	MODE=ind|PERS.=3|NOMB.=s	résista
.	.	PONfrt	MORPH=empty	.
