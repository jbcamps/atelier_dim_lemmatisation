— J’ai dans mon cœur un parc où s’égarent mes maux,
Des vases transparents où le lilas se fane,
Un scapulaire où dort le buis des saints rameaux,
Des flacons de poison et d’essence profane.
Des fruits trop tôt cueillis mûrissent lentement
En un coin retiré sur des nattes de paille,
Et l’arome subtil de leur avortement
Se dégage au travers d’une invisible entaille… 
— Et mon fixe regard qui veille dans la nuit
Sait un caveau secret que la myrrhe parfume,
Où mon passé plaintif, pâlissant et réduit,
Est un amas de cendre encor chaude qui fume. 
