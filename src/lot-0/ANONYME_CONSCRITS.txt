Allons, enfants des prolétaires,
On nous appelle au régiment ;
On veut nous faire militaires
Pour servir le gouvernement.
Nos pères furent très dociles
À des règlements incompris !
Nous, nous serons moins imbéciles,
Les insoumis (bis),

On nous dit d'avoir de la haine
Pour les Germains envahisseurs,
De tirer Alsace et Lorraine
D'entre les mains des oppresseurs ;
Que nous font les luttes guerrières
Des affameurs de tous pays ?
Nous ne voulons plus de frontières,
Les insoumis (bis),

On nous parle en vain de patrie,
Nous aimons les peuples divers ;
Nous allons porter l'Anarchie
Sur tous les points de l'Univers.
Au jour de la lutte finale,
Les réfractaires, tous unis,
Feront l'Internationale.
Des insoumis (bis),
